# mrs-2023-rmdok

## Name
Rukovalac multimedijalnim dokumentima

## Description
Predmetni projekat iz metodologije razvoja softvera. Ovo je javni projekat koji služi za razvoj protipa u toku vežbi za aplikaciju koju će studenti koristiti na svojim projektima.
Specifikacija projekta se može pronaći na timu predmeta, ali zarad konzistentnositi je dodata i u ovaj repozitorijum.

## Visuals
Za stvaranje uvida u to šta je ideja ovog editora, pogledati Rukovalac dokumentima - skica.pdf. Unutar dokumenta se nalaze slike prototipa koji je kreiran sa ciljem da vizuelno prikaže očekivanja po pitanju izgleda i funkcionalnosti ovog Rukovaoca dokumentima.

## Installation
Da bi se projekat mogao pokrenuti, potrebno je:
- Imati instaliran Python verzije 3.11.X
- Imati instaliranu biblioteku PySide6 za Python (po mogućstvu u virtuelnom okruženju)

## Usage
Pokrenuti main.py

## Roadmap
Plan realizacije je sledeći:
- Nedelja 1:
  -  Formirati osnovni prototip (srž GUI aplikacije).
  -  Prikazati model plugin framework-a uz poziv za razmišljanje za njegovu izmenu/dopunu.
  -  Navesti primere aplikacija koje imaju plugin mehanizam (VSCode, Astah, Eclipse, Chrome, ...).
  -  Navesti obavezne i potencijalne plugin-ove za rukovalac dokumentima.
- Nedelja 2:
  -  Definisati krajnju verziju plugin framework-a.
  -  Definisati sadržaj i format specifikacije meta-podataka plugin-a.
  -  Iskoristiti administraciju korisnika sa prethodnog projekta kao primer komponente.
  -  Postaviti milestone-ove na git-u i definisati početni skup zadataka, a na osnovu ovog plana.
- Nedelja 3:
  -  Konačno definisanje timova i objava spiska timova.
  -  Provera pristupa na svim definisanim projektima prijavljenih timova.
  -  Kreirati help komponentu.
- Nedelja 4:
  -  Kreirati komponentu za izbor uvoza radnog prostora i dokumenata (sistem datoteka ili baza podataka).
  -  Dopuna prototipa dodatnim widget-ima i sugestije pri nastavku razvoja prototipa.
  -  Konsultacije vezano za studentske projekte i njihov razvoj.
- Nedelja 5:
  -  Kontrola procesa razvoja aplikacije svih timova.
  -  Konsultacije u vezi procesa i proizvoda koji će se braniti u 6. nedelji.
  -  Dopuna prototipa i priprema za konačnu verziju.
- Nedelja 6:
  -  I kontrolna tačka projektnih zadataka studenata koji su u timovima.
- Nedelja 7:
  - Konsultacije u vezi sa projektnim zadacima i primenom odgovarajućih softverskih obrazaca.
- Nedelja 8:
  -Konsultacije u vezi projekta sa mr Aleksandra Mitrovic
  -Popravka Readme
  -Izbrisan .idea
- Nedelja 9:
  -Sastanak tima.
- Nedelja 10:
  -

## Contributing
Svi studenti koji žele da doprinesu kroz nove ideje i sugestije, to mogu učiniti na vežbama u toku razvoja same aplikacije.

## Authors and acknowledgment
Autor prototipa je: mr Aleksandra Mitrović (@aleksandramitrovic)

Autor specifikacije projektnog zadatka: prof. dr Branko Perišić (@bperisic)

## License
Ovo je projekat otvorenog koda. Dozvoljeno je preuzimanje i izmena istog.

## Project status
Projekat je u fazi razvoja sve do 6. nedelje nastave zimskog semestra u akademskoj 2023/2024 godini.
Nakon toga prelazi u stanje mirovanja i spreman je za preuzimanje od strane bilo kog lica.
