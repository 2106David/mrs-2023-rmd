class Context:
    def __init__(self, activated_plugins=[], workspace=""):
        self.activated_plugins = activated_plugins
        self.workspace = workspace

    @staticmethod
    def from_dict(context_dict):
        activated_plugins = context_dict.get("activated_plugins",[])
        workspace = context_dict.get("workspace", "")
        return Context(activated_plugins, workspace)