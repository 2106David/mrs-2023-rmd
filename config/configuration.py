class Configuration:
    def __init__(self, title, core_version, icon, window_size, plugins_path="plugins", context=""):
        self.title = title
        self.core_version = core_version
        self.icon = icon
        self.window_size = window_size
        
        # neobavezno polje
        self.plugins_path = plugins_path
        self.context = context

    @property
    def width(self):
        return self.window_size["width"]
    
    @property
    def height(self):
        return self.window_size["height"]
    
    @staticmethod
    def from_dict(configuration_dict):
        # Moze se desiti KeyError ako konfiguracija nije ispravna
        title = configuration_dict["title"]
        core_version = configuration_dict["core_version"]
        icon = configuration_dict["icon"]
        window_size = configuration_dict["window_size"]

        # zbog toga sto nije obavezan
        plugins_path = configuration_dict.get("plugins_path", "plugins")
        context = configuration_dict.get("context", "")

        return Configuration(title, core_version, icon, window_size, plugins_path, context)
    