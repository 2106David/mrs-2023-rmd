from PySide6 import QtWidgets, QtGui
from PySide6.QtGui import QFont, QPixmap

class InfoWidget(QtWidgets.QDialog):
    # FIXME: postaviti relativnu putanju
    config_path = "resources/data/configuration.json"
    def __init__(self, parent=None):
        super().__init__(parent)
        font = QFont()
        font.setBold(True)
        self._layout = QtWidgets.QVBoxLayout()
        self._name_label = QtWidgets.QLabel("Name:")
        self._authors_label = QtWidgets.QLabel("Authors:")
        self._version_label = QtWidgets.QLabel("Version:")
        self._name_label.setFont(font)
        self._authors_label.setFont(font)
        self._version_label.setFont(font)
   
        self._populate_layout()
        self.setLayout(self._layout)
        self.setWindowTitle("Rukovalac multimedijalnim dokumentima Help")
        self.resize(800, 600)


    def _populate_layout(self):
        # FIXME: procitati podatke iz konfiguracije i prepisati stringove (labele)
        self._layout.addWidget(self._name_label)
        self._layout.addWidget(QtWidgets.QLabel("Rukovalac multimedijalnim dokumentima"))
        self._layout.addWidget(self._authors_label)

        ivana_image = QtGui.QPixmap(r"resources\help_photo\Ivana.jpg")  # Replace with the actual image file path
        nedo_image = QtGui.QPixmap(r"resources\help_photo\Nedo.jpg")  # Replace with the actual image file path
        daniel_image = QtGui.QPixmap(r"resources\help_photo\Daniel.jpg")  # Replace with the actual image file path
        david_image = QtGui.QPixmap(r"resources\help_photo\David.jpg")  # Replace with the actual image file path

        ivana_label = QtWidgets.QLabel()
        ivana_label.setPixmap(ivana_image)
        ivana_label.setFixedSize(100, 100)
        nedo_label = QtWidgets.QLabel()
        nedo_label.setPixmap(nedo_image)
        daniel_label = QtWidgets.QLabel()
        daniel_label.setPixmap(daniel_image)
        david_label = QtWidgets.QLabel()
        david_label.setPixmap(david_image)

        self._layout.addWidget(QtWidgets.QLabel("Ivana Cvijetinović"))
        self._layout.addWidget(ivana_label)
        self._layout.addWidget(QtWidgets.QLabel("Nedo Panić"))
        self._layout.addWidget(nedo_label)
        self._layout.addWidget(QtWidgets.QLabel("Daniel Đuriš"))
        self._layout.addWidget(daniel_label)
        self._layout.addWidget(QtWidgets.QLabel("David Kovačić"))
        self._layout.addWidget(david_label)
        self._layout.addWidget(self._version_label)
        self._layout.addWidget(QtWidgets.QLabel("1.0.0"))
