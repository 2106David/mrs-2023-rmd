import os
import json
from PySide6.QtWidgets import QDockWidget
from PySide6 import QtWidgets
from PySide6.QtCore import Signal, Qt
from plugins.workspace_plugin.model.workspace_manager_model import WorkspaceManagerModel
from plugins.workspace_plugin.controller.workspace_controller import WorkspaceController
 
class WorkspaceManager(QDockWidget):
    clicked_signal = Signal(str)
 
    def __init__(self, parent=None) -> None:
        super().__init__("Workspace Manager", parent)
 
        self.iface = parent
 
    def populate(self, path=None):
        if not path:
            self.path = os.path.realpath(
                os.path.join(
                    os.path.dirname(__file__),
                    "..\..\..",
                    "data",
                    "radni_prostor.ws"
                )
            )
        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)
        self.tabs = QtWidgets.QTabWidget(self)
        self.layout.addWidget(self.tabs)
 
        self.model = WorkspaceManagerModel("Workspace")
        self.controller = WorkspaceController(self.iface, self, self.model)
        self.controller.addActions()
 
        self.tree_widget = QtWidgets.QTreeWidget(self.tabs)
        self.tree_widget.setHeaderLabel("Workspace")
 
        with open(self.path) as file:
            self.document = json.load(file)
            self.controller.make_model(self.document, self.model.root)
        self.tree_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tree_widget.customContextMenuRequested.connect(
            self.controller._show_context_menu
        )
 
        self.tree_widget.setHeaderHidden(True)
        for i in range(1, 5):
            self.tree_widget.hideColumn(i)
 
        self.controller.show_model(self.model.root)
 
        self.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.tabs.addTab(self.tree_widget, "Active")
        self.setWidget(self.tabs)