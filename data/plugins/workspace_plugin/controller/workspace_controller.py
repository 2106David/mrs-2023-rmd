import os
from PySide6 import QtWidgets
from PySide6.QtWidgets import QFileDialog, QTreeWidgetItem, QMenu
from PySide6.QtGui import QIcon, Qt, QAction
import json
from plugins.workspace_plugin.model.workspace_manager_model import Node
 
 
class WorkspaceController:
    def __init__(self, parent, view, model):
        self.parent = parent
        self.view = view
        self.model = model
 
    def isActionAdded(self, action_text):
        for action in self.parent.tool_bar.actions():
                if action.text() == action_text:
                    return True
        return False
    
    def addActions(self):
        if not self.isActionAdded("Open workspace"):
            open_file = QAction(QIcon("resources/Ikonice/add.png"), "Add workspace", self.parent) 
            open_file.setStatusTip("Open Workspace")
            open_file.triggered.connect(self.triggerToolBarSpecOpenWorkspace)
            self.parent.tool_bar.addAction(open_file)

        if not self.isActionAdded("Remove workspace"):
            save_data = QAction(QIcon("resources/Ikonice/minus.png"), "Delete workspace", self.parent) 
            save_data.setStatusTip("Remove Workspace")
            save_data.triggered.connect(self.triggerToobarRemoveWorkspace)
            self.parent.tool_bar.addAction(save_data)
 
    def triggerToolBarSpecOpenWorkspace(self):
        self.dialog = QFileDialog()
        self.dialog.setWindowTitle("Open workspace")
        self.dialog.setFileMode(QFileDialog.AnyFile)
        self.dialog.setNameFilter("Text files (*.ws)")
        if self.dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.filename = self.dialog.selectedFiles()
        if self.filename:
            self.fname = str(self.filename[0])
            self.view.populate(self.fname)
 
    def triggerToobarRemoveWorkspace(self, model):
        print("Workspace removed")
 
    def make_model(self, dictionary, root_item):
        if type(dictionary) is dict:
            for i in dictionary:
                if i.startswith(("Kolekcija", "Dokument")):
                    node_type = 0 if i.startswith("Kolekcija") else 1
                    node = Node(i, node_type, root_item)
                    root_item.add_child(node)
                    self.make_model(dictionary[i], node)
            return
        return
 
    def content_clicked(self, item, action):
        if self.model.find_node(item.data(0, 0)).type == 1:
            temp = self.model.find_node(item.data(0, 0))
 
            path = []
            while temp.parent:
                path.append(temp.value)
                temp = temp.parent
 
            path = reversed(path)
 
            content = self.view.document
            for path_part in path:
                content = content[path_part]
            message = content
            message["id"] = item.data(0, 0)
            self.view.message = message
            self.view.clicked_signal.emit(action)
 
    def show_model(self, current_node, widget_item_parent=None):
        if current_node.children:
            for child in current_node.children:
                t = QTreeWidgetItem()
                t.setData(0, 0, child.value)
                if current_node == self.model.root:
                    self.view.tree_widget.addTopLevelItem(t)
                else:
                    widget_item_parent.addChild(t)
                self.show_model(child, t)
        return
 
    def _show_context_menu(self, position):
        display_action1 = QAction(QIcon("resources/Ikonice/folder.png"), "Open")
        display_action2 = QAction(QIcon("resources/Ikonice/edit.png"), "Rename")
        display_action3 = QAction(QIcon("resources/Ikonice/trash.png"),"Remove")
        display_action1.triggered.connect(self.open_file)
        display_action2.triggered.connect(self.rename_file)
        display_action3.triggered.connect(self.remove_file)
        menu = QMenu(self.parent)
        menu.addAction(display_action1)
        menu.addAction(display_action2)
        menu.addAction(display_action3)
        menu.exec_(self.view.tree_widget.mapToGlobal(position))
 
    def open_file(self):
        column = self.view.tree_widget.currentColumn()
        text = self.view.tree_widget.currentItem().text(column)
        iterator = QtWidgets.QTreeWidgetItemIterator(self.view.tree_widget)
 
        while iterator.value():
            item = iterator.value()
            if item.text(0) == text:
                parent = item.parent()
                if parent is not None:
                    self.content_clicked(self.view.tree_widget.currentItem(), "open")
                else:
                    print("Collections cannot be opened")
            iterator += 1
 
    def rename_file(self):
        column = self.view.tree_widget.currentColumn()
        text = self.view.tree_widget.currentItem().text(column)
        iterator = QtWidgets.QTreeWidgetItemIterator(self.view.tree_widget)
 
        while iterator.value():
            item = iterator.value()
            if item.text(0) == text:
                parent = item.parent()
                if parent is not None:
                    item.setSelected(True)
                    item.setFlags(item.flags() | Qt.ItemIsEditable)
                    self.view.tree_widget.editItem(item)
                else:
                    print("Collections cannot be renamed")
            iterator += 1
 
    def remove_file(self):
        column = self.view.tree_widget.currentColumn()
        text = self.view.tree_widget.currentItem().text(column)
        iterator = QtWidgets.QTreeWidgetItemIterator(self.view.tree_widget)
 
        while iterator.value():
            item = iterator.value()
            if item.text(0) == text:
                parent = item.parent()
                if parent is not None:
                    parent.takeChild(0)
                else:
                    print("Collections cannot be deleted")
            iterator += 1
 