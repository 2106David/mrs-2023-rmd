from ..view.tabSingle import TabSingle


class DocumentController:
    def __init__(self, parent, view, model):
        self.parent = parent

        self.view = view
        self.model = model
        
    def process(self, action):
        match action:
            case "open":
                self.document = self.parent.workspace.message
                flag = True
                for i in range(self.view.count()):
                    if self.view.widget(i).objectName() == self.document["id"]:
                        self.view.setCurrentIndex(i)
                        flag = False
                        break
                if flag:
                    self.model.listOfOpenedDocuments.append(self.document["id"])

                    doc = TabSingle(self.parent, self.document)
                    self.view.addTab(doc, self.document["id"])
                    self.view.setCurrentIndex(self.view.count() - 1)
            case _:
                pass

