from PySide6.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QHBoxLayout, QToolBar, QListWidget, QTreeWidget, QListView, QGridLayout
from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QIcon, QAction

from ..model.document import Document


class TabSingle(QWidget):
    def __init__(self, parent=None, doc = None):
        super().__init__(parent=parent)

        self.setObjectName(doc["id"])

        self.layout_type = QVBoxLayout(self)
    
        self.original = Document(doc)
        self.model = Document(doc)

        self.upper_part = QWidget(self)
        self.upper_part.layout_type = QHBoxLayout()

        self.content_space = QTabWidget(self)

        self.content_tab = QTreeWidget(self.content_space)
        self.content_tab.setHeaderHidden(True)
        self.content_space.setMaximumWidth(165)

        self.view_tab = QListWidget(self.content_space)
        self.view_tab.setViewMode(QListView.IconMode)
        self.view_tab.setIconSize(QSize(130, 130))
        self.view_tab.setMovement(QListView.Static)
        self.view_tab.setWrapping(False)
        self.view_tab.setFlow(QListView.TopToBottom)

        copy = QAction(QIcon("resources/Ikonice/copy.png"),"Copy", self)
        copy.triggered.connect(self.copy)
        cut = QAction(QIcon("resources/Ikonice/scissors.png"),"Cut", self)
        cut.triggered.connect(self.cut)
        paste = QAction(QIcon("resources/Ikonice/paste.png"),"Paste", self)
        paste.triggered.connect(self.paste)

        left = QAction(QIcon("resources/Ikonice/align-left.png"),"Copy", self)
        left.triggered.connect(self.left)
        center = QAction(QIcon("resources/Ikonice/format.png"),"Copy", self)
        center.triggered.connect(self.center)
        justify = QAction(QIcon("resources/Ikonice/justification.png"),"Copy", self)
        justify.triggered.connect(self.justify)
        right = QAction(QIcon("resources/Ikonice/align-right.png"),"Copy", self)
        right.triggered.connect(self.right)

        addImg = QAction(QIcon("resources/Ikonice/add-image.png"),"Add image", self)
        addImg.triggered.connect(self.addImg)


        self.toolbar = QToolBar()
        self.toolbar.layout_type = QVBoxLayout()        
        self.toolbar.addAction(copy)
        self.toolbar.addAction(cut)
        self.toolbar.addAction(paste)
        self.toolbar.addSeparator
        self.toolbar.addAction(left)
        self.toolbar.addAction(center)
        self.toolbar.addAction(justify)
        self.toolbar.addAction(right)
        self.toolbar.addSeparator
        self.toolbar.addAction(addImg)
      
        self.page_space = QWidget(self)
        self.page_space.layout_type = QGridLayout() 
        self.page_space.setLayout(self.page_space.layout_type)
        self.page_space.setStyleSheet("border:1px solid rgb(200, 200, 200); ") 
        
        self.upper_part.layout_type.addWidget(self.content_space, Qt.Alignment(1))
        self.upper_part.layout_type.addWidget(self.page_space, Qt.Alignment(2))

        self.upper_part.setLayout(self.upper_part.layout_type)

        self.slot_space = QTabWidget(self)

        self.setLayout(self.layout_type)

        self.layout_type.addWidget(self.toolbar)
        self.layout_type.addWidget(self.upper_part)
        self.layout_type.addWidget(self.slot_space)
        
    def copy (self):
         print("Copy")

    def paste(self):
        print("Paste")

    def cut(self):
        print("Cut")

    def left(self):
        print("poravnato lijevo")

    def right(self):
        print("poravnato desno")

    def center(self):
        print("poravnato centar")

    def justify(self):
        print("poravnato obostrano")

    def addImg(self):
        print("slika dodana")

