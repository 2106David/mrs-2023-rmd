import random

from PySide6.QtWidgets import QAction, QListWidgetItem, QTreeWidgetItem, QMessageBox, QTreeWidgetItemIterator
from PySide6.QtGui import QIcon, QPixmap

from ..model.page_model import Page
from page_handler.view.page_view import PageView


class DocumentController():
    def __init__(self, iface, view, model):
        self.iface = iface
        self.view = view
        self.model = model

    # odgovor na klik u sadrzaju
    def content_clicked(self, item):
        '''
        :param item: objekat drveta, sadrzi naslov u prvoj koloni i id stranice u drugoj koloni
        '''
        value = item.data(1, 0)
        self.render_page(self.model.pages.get_node_by_value(value).value)

    # odgovor na klik u slikovnom prikazu
    def view_clicked(self, item):
        '''
        :param item: objekat drveta, sadrzi naslov u prvoj koloni i id stranice u drugoj koloni
        '''
        index = self.view.view_tab.indexFromItem(item).row()
        self.render_page(self.model.pages.get_node_by_index(index).value)

    # napravi akciju od parametara
    def make_action(self, icon_path, status_tip_name, parent, method_name):
        temp = QAction(QIcon(icon_path), status_tip_name, parent)
        temp.setStatusTip(status_tip_name)
        temp.triggered.connect(method_name)
        temp.setObjectName(status_tip_name)
        if status_tip_name == "Previous page":
            temp.setDisabled(True)

        parent.addAction(temp)

    def refresh(self):
        self.load_tree_widget()
        self.load_view_widget()

    def load_tree_widget(self):
        for i in reversed(range(self.view.content_tab.topLevelItemCount())):
            self.view.content_tab.takeTopLevelItem(i)
        
        self.model.iterator.first()
        for i in self.model.iterator:
            t = QTreeWidgetItem()
            t.setData(0,0,i.value.id)
            t.setData(1,0,i.value.id)
            self.view.content_tab.addTopLevelItem(t)
    
    def load_view_widget(self):
        for i in reversed(range(self.view.view_tab.count())):
            self.view.view_tab.takeItem(i)

        self.model.iterator.first()
        for i in self.model.iterator:
            iconica = QIcon()

            #FIXME ovo bi trebalo da gleda layout a ne slots
            #komponenta za stranice mora azurirati layout svojstvo kako bi to bilo moguce
            #komponenta za stranice mora pozivati load_view_widget kako bi se promena prikazala u view
            match str(len(i.value.slots)):
                case "0":
                    iconica.addPixmap(QPixmap("resources\layouts\layout_0.png"), QIcon.Normal, QIcon.Off)

                case "1":
                    iconica.addPixmap(QPixmap("resources\layouts\layout_1.png"), QIcon.Normal, QIcon.Off)
                
                case "2":
                    iconica.addPixmap(QPixmap("resources\layouts\layout_2.png"), QIcon.Normal, QIcon.Off)
                
                case "3":
                    iconica.addPixmap(QPixmap("resources\layouts\layout_3.png"), QIcon.Normal, QIcon.Off)
                
                case "4":
                    iconica.addPixmap(QPixmap("resources\layouts\layout_4.png"), QIcon.Normal, QIcon.Off)
                case _:
                    pass
                    
            temp = QListWidgetItem()
            temp.setIcon(iconica)
            temp.setStatusTip(i.value.id)
            self.view.view_tab.addItem(temp)

    def save_document(self, is_checked):
        print("save document")

    def add_page(self, is_checked):
        id = "ID" + str(len(self.model.pages))+ str(random.randint(0,50))
        self.model.add_page(Page(id))

        self.load_tree_widget()
        self.load_view_widget()

    def remove_page(self, is_checked):
        if self.model.iterator.has_next():
            self.model.current_page = self.model.iterator.next().value
        elif self.model.iterator.has_previous():
            self.model.current_page = self.model.iterator.previous().value
        else:
            self.model.current_page = None
            
        self.model.remove_page("FIXME")
        for i in self.model.iterator:
            print(i.value)

        self.load_tree_widget()
        self.load_view_widget()
        print("Remove page")

    def share_page(self, is_checked):
        # TODO
        print("Share page")

    def go_to_next_page(self, is_checked):
        if self.model.current_page:
            if self.model.iterator.has_next():
                page = self.model.iterator.next().value
                self.render_page(page)
            else:
                # TODO show message
                QMessageBox.warning(self.view, "Navigation error", "Reached the end of the document!")
        else:
            self.go_to_first_page(False)

    def go_to_previous_page(self, is_checked):
        if self.model.iterator.has_previous():
            page = self.model.iterator.previous().value
            self.render_page(page)
        else:
            # TODO show message
            print("Error; can not go to the previous page")

    def go_to_first_page(self, is_checked):
        page = self.model.iterator.first().value
        self.render_page(page)

    def go_to_last_page(self, is_checked):
        page = self.model.iterator.last().value
        self.render_page(page)

    def render_page(self, page):
        if self.page_plugin_active() and self.model.current_page != page:
            self.model.iterator.set_current(page.id)
            if self.model.new:
                self.page_view = PageView(page, self.view.page_space, self.view.slot_space, self.view)
                self.model.new = False
            else:
                self.page_view.refresh(page)
            self.model.current_page = page

            iterator = QTreeWidgetItemIterator(self.view.content_tab)
            while iterator.value():
                item = iterator.value()
                item.setSelected(self.check_for_selection(item.data(1,0)))
                iterator += 1
            
            for i in range(self.view.view_tab.count()):
                item = self.view.view_tab.item(i)
                item.setSelected(self.check_for_selection(item.statusTip()))

            for action in self.view.nav_space.actions():
                if action.objectName() == "Next page":
                    action.setDisabled(self.check_action_for_disabling(self.model.iterator.has_next()))
                if action.objectName() == "Previous page":
                    action.setDisabled(self.check_action_for_disabling(self.model.iterator.has_previous()))
           
    def check_action_for_disabling(self, parameter):
        if not parameter:
            return True
        return False

    def check_for_selection(self,to_compare):
        if to_compare == self.model.current_page.id:
            return True
        return False

    def page_plugin_active(self):
        if [True for plugin in self.iface.plugin_registry._plugins if str(plugin.plugin_specification.id) == "2020271067" and plugin.activated]:
            return True
