class Slot:
    def __init__(self, index, header = "", path = None):
        self.index = index
        self.header = header
        self.path = path
    
    def __str__(self) -> str:
        if self.path:
            return self.index + ". Header: " + self.header + " Path: " + self.path
        else:
            return self.index + ". Header: " + self.header + " Path: empty" 