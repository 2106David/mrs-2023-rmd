# from plugins.document_handler.model.document_model import Document
# from plugins.document_handler.model.page_model import Page
# from plugins.document_handler.model.slot_model import Slot


class Node:
    def __init__(self, value = None):
        self.value = value
        self.next = None
        self.prev = None
    
    def __str__(self):
        return self.value

class DLinkedList():
    def __init__(self):
        self.head = None
    
    def __len__(self):
        count = 0
        node = self.head
        while node and node.next:
            node = node.next
            count += 1
        
        return count

    def append(self, value):
        new = Node(value)
        if self.head is None:
            self.head = new
            return

        last = self.get_last()
        last.next = new
        new.prev = last

    def get_last(self):
        node = self.head
        while node and node.next:
            node = node.next
        
        return node

    #FIXME
    def remove_node(self, value):
        if self.head:
            node = self.head
            while node:
                if node.value in [value]:
                    if node.prev:
                        node.prev.next = node.next
                    if node.next:
                        node.next.prev = node.prev
                    if not(node.next or node.prev):
                        self.head.next = None
                        self.head.prev = None
                    return
                node = node.next
    
    def remove_nodes(self, *values):
        if self.head:
            node = self.head
            while node:
                if node.value in values:
                    node.prev.next = node.next
                    if node.next:
                        node.next.prev = node.prev
                node = node.next


    def remove_from_head(self):
        if self.head is None:
            print("List is empty, nothing to delete")
            return
        if self.head.next is None:
            self.head = None
            return
        
        self.head = self.head.next
        self.head.prev = None

    def remove_from_tail(self):
        if self.head is None:
            print("List is empty, nothing to delete")
            return
        if self.head.next is None:
            self.head = None
            self.tail = None
            return
        
        last = self.get_last()
        last.prev.next = None

    def get_node_by_index(self, index):
        node = self.head
        if self.head:
            ind = 0
            while ind is not index and node:
                ind += 1
                node = node.next
        return node

    def get_node_by_value(self, value):
        node = self.head
        while node:
            if value == node.value.id:
                return node
            node = node.next
        return node


    def __str__(self):
        if not self.head:
            return "List is empty."
        
        node = self.head
        priv = []
        while node:
            priv.append(node.value)
            node = node.next
        return str(priv)
    

