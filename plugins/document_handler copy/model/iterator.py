from collections.abc import Iterable

class DocumentIterator(Iterable):
    def __init__(self, d_list):
        self.d_list = d_list
        self.node = self.d_list.head

    def __iter__(self):
        return self

    def __next__(self):
        if self.node:
            result = self.node
            self.node = self.node.next
            return result
        else:
            raise StopIteration

    def current(self):
        return self.node

    def has_next(self):
        if self.node and self.node.next:
            return True
        return False

    def next(self):
        if self.node.next:
            result = self.node.next
            self.node = self.node.next
            return result
        else:
            raise StopIteration
        
    def set_current(self,value):
        node = self.d_list.head
        while node:
            if value == node.value.id:
                self.node = node
                return
            node = node.next
        return
    
    def has_previous(self):
        if self.node and self.node.prev:
            return True
        return False

    def previous(self):
        if self.node.prev:
            result = self.node.prev
            self.node = self.node.prev
            return result
        else:
            raise StopIteration
    
    def is_empty(self):
        if self.d_list.head:
            return True
        return False

    def first(self):
        if self.d_list.head:
            self.node = self.d_list.head
            return self.d_list.head
        return None
    
    def last(self):
        if self.d_list.head:
            node = self.d_list.head
            while node.next:
                node = node.next
            self.node = node
            return node
        return None