from ..model.doubly_linked_list import DLinkedList
from ..model.iterator import DocumentIterator
from ..model.page_model import Page
from ..model.slot_model import Slot



class Document():
    def __init__(self, document, current_page = None):
        self.id = document["id"]
        self.new = True
        self.current_page = current_page
        self.pages = DLinkedList()
        for p in document["pages"]:
            slots = []
            for slot in document["pages"][p]["slots"]:
                try:
                    s = Slot(slot, document["pages"][p]["slots"][slot]["header"], document["pages"][p]["slots"][slot]["path"])
                except:
                    s = Slot(slot)
                slots.append(s)

            page = Page(p, document["pages"][p]["layout"], slots)

            self.pages.append(page)
        self.iterator = DocumentIterator(self.pages)
    
    def add_page(self, page):
        self.pages.append(page)
    
    def remove_page(self, page_id):
        # self.pages.remove_node(page_id)
        self.pages.remove_from_head()
        self.iterator = DocumentIterator(self.pages)
        
    @property
    def get_pages(self):
        return self.pages