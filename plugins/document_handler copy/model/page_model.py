class Page:
    def __init__(self, id, layout = 0, slots = []):
        self.id = id
        self.layout = layout
        self.slots = []
        for slot in slots:
            self.slots.append(slot)
    
    def __eq__(self, __o: object) -> bool:
        try:
            return self.id == __o.id
        except:
            return False

    def __str__(self) -> str:
        return "PageID: " + str(self.id) + " Layout: " + str(self.layout) + " Num. of slots: " + str(len(self.slots))
