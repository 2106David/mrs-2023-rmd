from PySide6.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QHBoxLayout, QToolBar, QListWidget, QListWidgetItem, QTreeWidget, QTreeWidgetItem, QListView, QGridLayout, QTreeWidgetItemIterator
from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QIcon, QPixmap

from ..model.document_model import Document
from ..controller.document_controller import DocumentController


class TabSingle(QWidget):
    def __init__(self, parent=None, doc = None):
        super().__init__(parent=parent)

        self.setObjectName(doc["id"])

        self.layout_type = QVBoxLayout(self)

        print("original")
        self.original = Document(doc)
        print("kopija")
        self.model = Document(doc)
        self.controller = DocumentController(self.parent(), self, self.model)

        self.upper_part = QWidget(self)
        self.upper_part.layout_type = QHBoxLayout()

        # ovo su navigacioni sistemi - sadrzaj i prikaz
        self.content_space = QTabWidget(self)

        self.content_tab = QTreeWidget(self.content_space)
        self.content_tab.setHeaderHidden(True)
        self.content_space.setMaximumWidth(165)

        self.controller.load_tree_widget()
        self.content_tab.itemClicked.connect(self.controller.content_clicked)


        self.view_tab = QListWidget(self.content_space)
        self.view_tab.setViewMode(QListView.IconMode)
        self.view_tab.setIconSize(QSize(130, 130))
        self.view_tab.setMovement(QListView.Static)
        self.view_tab.setWrapping(False)
        self.view_tab.setFlow(QListView.TopToBottom)

        self.controller.load_view_widget()
        self.view_tab.itemClicked.connect(self.controller.view_clicked)

        self.content_space.addTab(self.content_tab, QIcon(), 'Contents')
        self.content_space.addTab(self.view_tab, QIcon(), 'View')

        self.nav_space = QToolBar()
        self.nav_space.layout_type = QVBoxLayout()

        self.controller.make_action("resources/icons/save.png", "Save document", self.nav_space, self.controller.save_document)
        self.controller.make_action("resources/icons/refresh.png", "Refresh document", self.nav_space, self.controller.refresh)
        self.nav_space.addSeparator()
        self.controller.make_action("resources/icons/add_page.png", "Add page", self.nav_space, self.controller.add_page)
        self.controller.make_action("resources/icons/remove_page.png", "Remove page", self.nav_space, self.controller.remove_page)
        self.controller.make_action("resources/icons/share_page.png", "Share page", self.nav_space, self.controller.share_page)
        self.nav_space.addSeparator()
        self.controller.make_action("resources/icons/first.png", "First page", self.nav_space, self.controller.go_to_first_page)
        self.controller.make_action("resources/icons/previous.png", "Previous page", self.nav_space, self.controller.go_to_previous_page)
        self.controller.make_action("resources/icons/next.png", "Next page", self.nav_space, self.controller.go_to_next_page)
        self.controller.make_action("resources/icons/last.png", "Last page", self.nav_space, self.controller.go_to_last_page)
        

        self.page_space = QWidget(self)
        self.page_space.layout_type = QGridLayout() 
        self.page_space.setLayout(self.page_space.layout_type) #rm
        self.page_space.setStyleSheet("border:1px solid rgb(200, 200, 200); ") #rm
        
        self.upper_part.layout_type.addWidget(self.content_space, Qt.Alignment(1))
        self.upper_part.layout_type.addWidget(self.page_space, Qt.Alignment(2))

        self.upper_part.setLayout(self.upper_part.layout_type)

        self.slot_space = QTabWidget(self)

        self.setLayout(self.layout_type)

        self.layout_type.addWidget(self.nav_space)
        self.layout_type.addWidget(self.upper_part)
        self.layout_type.addWidget(self.slot_space)
