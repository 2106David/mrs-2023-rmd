from PySide6.QtWidgets import QTabWidget, QWidget

from plugins.document_handler.controller.document_controller import (
    DocumentController,
)

from plugins.document_handler.model.model import DocumentModel


class DocumentView(QTabWidget):
    def __init__(self, parent=None):
        super(DocumentView, self).__init__(parent)
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.removeTab)

        self.model = DocumentModel([], [])

        self.controller = DocumentController(self.parent(), self, self.model)

        self.parent().whole_doc_controller = self.controller
        self.parent().workspace.clicked_signal.connect(self.controller.process)

    @property
    def tabs(self):
        ret_tabs = []
        for i in range(self.count()):
            ret_tabs.append(self.tabText(i))
        return ret_tabs

    def get_index(self, tab_name):
        try:
            return self.tabs.index(tab_name)
        except ValueError:
            return None

    def new_tab(self, file_name):
        tab_name = file_name
        if tab_name not in self.tabs:
            self.workspace_tab_widget = QWidget()
            self.addTab(self.workspace_tab_widget, tab_name)
            self.setCurrentIndex(self.currentIndex() + 1)
        else:
            index = self.get_index(tab_name)
            self.setCurrentIndex(index)
