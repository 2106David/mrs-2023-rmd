from plugin_framework.extension import Extension
import json
from plugins.document_handler.view.view import DocumentView


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)

    def activate(self):
        self.iface.document_view = DocumentView(self.iface)
        self.iface.setCentralWidget(self.iface.document_view)
        self.activated = True
        try:
            if self.plugin_specification.dependencies:
                for i in self.plugin_specification.dependencies:
                    for plugin in self.iface.plugin_registry._plugins:
                        if plugin.plugin_specification.id == i.id and not plugin.activated:
                            plugin.activate()
            with open("resources/data/context.json", "r+") as f:
                data = json.load(f)
                if super().id not in data["activated_plugins"]:
                    data["activated_plugins"].append(super().id)
                f.seek(0)
                json.dump(data, f, indent=4)
                f.truncate()
        
        except:
            print(" ")
            
    def deactivate(self):
        # kopmonenta za rad sa dokumentom kao celinom proverava ovaj parametar pri otvaranju dokumenta
        self.activated = False

        with open("resources/data/context.json", "r+") as f:
            data = json.load(f)
            data["activated_plugins"].remove(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()
        


   
        