from PySide6 import QtGui
from plugin_framework.extension import Extension
from .widgets.info_widget import InfoWidget
import json

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        # TODO: ukoliko u nekom plugin-u treba sacuvati referencu na iface, napraviti atribut
        self.widget = InfoWidget(iface)
        self.open_action = QtGui.QAction("&About")
        self.open_action.triggered.connect(self.open_help)
        
    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.iface.add_menu_action("&Help", self.open_action)
        self.activated = True
        # print("Activated")
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            if super().id not in data["activated_plugins"]:
                data["activated_plugins"].append(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()


    def deactivate(self):
        self.iface.remove_menu_action("&Help", self.open_action)
        self.activated = False
        # print("Deactivated")
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            data["activated_plugins"].remove(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()


    def open_help(self):
        self.widget.show()