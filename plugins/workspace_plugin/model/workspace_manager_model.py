class WorkspaceManagerModel:
    def __init__(self, workspace_name="") -> None:
        self.root = Node(workspace_name, 0)

    def find_node(self, value, current_node=None, za_obraditi=[]):
        if not current_node:
            current_node = self.root

        for child in current_node.children:
            za_obraditi.append(child)

        if str(current_node.value) == str(value):
            return current_node
        for current_node in za_obraditi:
            za_obraditi.remove(current_node)
            return self.find_node(value, current_node, za_obraditi)


class Node:
    def __init__(self, value, type, parent=None, children=[]):
        self.value = value
        self.type = type

        self.parent = parent
        self.children = []
        for child in children:
            self.children.append(child)

    def add_child(self, child_node):
        self.children.append(child_node)
