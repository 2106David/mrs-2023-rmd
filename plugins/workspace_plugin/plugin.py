import json
from PySide6.QtCore import Qt
from plugin_framework.extension import Extension
from plugins.workspace_plugin.view.workspace_manager import WorkspaceManager


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)

    def activate(self):
        if not self.activated:
            self.iface.workspace = WorkspaceManager(self.iface)
            self.iface.workspace.populate()
            self.iface.workspace.resize(500, 600)

            self.iface.addDockWidget(
                Qt.LeftDockWidgetArea, self.iface.workspace, Qt.Vertical
            )

            self.activated = True

       
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            if super().id not in data["activated_plugins"]:
                data["activated_plugins"].append(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()

    def deactivate(self):
        # FIXME: za sad hard kodovano
        for a in self.iface.tool_bar.actions():
            if a.text() == "Add workspace":
                self.iface.tool_bar.removeAction(a)
            if a.text() == "Delete workspace":
                self.iface.tool_bar.removeAction(a)
        self.iface.removeDockWidget(self.iface.workspace)
        self.activated = False

       
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            data["activated_plugins"].remove(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()
