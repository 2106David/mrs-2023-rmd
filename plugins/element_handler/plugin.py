import json
from PySide6.QtCore import Qt
from plugin_framework.extension import Extension



class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)

    def activate(self):
        
        self.activated = True
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            if super().id not in data["activated_plugins"]:
                data["activated_plugins"].append(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()

    def deactivate(self):
        self.activated = False
        with open("resources\data\context.json", "r+") as f:
            data = json.load(f)
            data["activated_plugins"].remove(super().id)
            f.seek(0)
            json.dump(data, f, indent=4)
            f.truncate()

