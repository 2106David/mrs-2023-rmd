import sys
from json import load
from PySide6 import QtWidgets
from core.main_window import MainWindow
from config.configuration import Configuration
from config.context import Context
from plugin_framework.plugin_registry import PluginRegistry

def load_configuration():
    # putanja do konfiguracije uvek mora biti ista
    config_path = "resources/data/configuration.json"
    with open(config_path, encoding="UTF-8") as file:
        config_data = load(file)
        # FIXME: Ovde moze nastati izuzetak
        config = Configuration.from_dict(config_data)
        return config


def load_context(path):
    with open(path, encoding="UTF-8") as file:
        context_data = load(file)
        context = Context.from_dict(context_data)
        return context

if __name__ == "__main__":
    application = QtWidgets.QApplication(sys.argv)

    # TODO: konfiguraciju ucitati iz datoteke
    configuration = load_configuration()

    context = load_context(configuration.context)

    # TODO: prikazati glavni prozor
    main_window = MainWindow(configuration)

    # inicijalizacija registra svih plugin-onva
    plugin_registry = PluginRegistry(main_window, configuration.plugins_path, context.activated_plugins)

    main_window.add_plugin_registry(plugin_registry)
    
    main_window.show()

    # Pre, u verziji PySide2 smo pozivali exec_(), no uskoro ce biti uklonjena, pa se koristi exec()
    sys.exit(application.exec())


